use super::*;
use glutin;

impl From<glutin::Event> for Event {
    fn from(x: glutin::Event) -> Self {
        match x {
            glutin::Event::Resized(a, b) => Event::Resized(a.into(), b.into()),
            glutin::Event::Moved(a, b) => Event::Moved(a.into(), b.into()),
            glutin::Event::Closed => Event::Closed,
            glutin::Event::DroppedFile(a) => Event::DroppedFile(a.into()),
            glutin::Event::ReceivedCharacter(a) => Event::ReceivedCharacter(a.into()),
            glutin::Event::Focused(a) => Event::Focused(a.into()),
            glutin::Event::KeyboardInput(a, b, c) => Event::KeyboardInput(a.into(), b.into(), c.map(|c| c.into())),
            glutin::Event::MouseMoved(a, b) => Event::MouseMoved(a.into(), b.into()),
            glutin::Event::MouseWheel(a, b) => Event::MouseWheel(a.into(), b.into()),
            glutin::Event::MouseInput(a, b) => Event::MouseInput(a.into(), b.into()),
            glutin::Event::TouchpadPressure(a, b) => Event::TouchpadPressure(a.into(), b.into()),
            glutin::Event::Awakened => Event::Awakened,
            glutin::Event::Refresh => Event::Refresh,
            glutin::Event::Suspended(a) => Event::Suspended(a.into()),
            glutin::Event::Touch(a) => Event::Touch(a.into()),
        }
    }
}

impl From<glutin::TouchPhase> for TouchPhase {
    fn from(x: glutin::TouchPhase) -> Self {
        match x {
            glutin::TouchPhase::Started => TouchPhase::Started,
            glutin::TouchPhase::Moved => TouchPhase::Moved,
            glutin::TouchPhase::Ended => TouchPhase::Ended,
            glutin::TouchPhase::Cancelled => TouchPhase::Cancelled,
        }
    }
}

impl From<glutin::Touch> for Touch {
    fn from(x: glutin::Touch) -> Self {
        Touch{ phase: x.phase.into(), location: x.location.into(), id: x.id.into() }
    }
}

impl From<glutin::ElementState> for ElementState {
    fn from(x: glutin::ElementState) -> Self {
        match x {
            glutin::ElementState::Pressed => ElementState::Pressed,
            glutin::ElementState::Released => ElementState::Released,
        }
    }
}

impl From<glutin::MouseButton> for MouseButton {
    fn from(x: glutin::MouseButton) -> Self {
        match x {
            glutin::MouseButton::Left => MouseButton::Left,
            glutin::MouseButton::Right => MouseButton::Right,
            glutin::MouseButton::Middle => MouseButton::Middle,
            glutin::MouseButton::Other(a) => MouseButton::Other(a),
        }
    }
}

impl From<glutin::MouseScrollDelta> for MouseScrollDelta {
    fn from(x: glutin::MouseScrollDelta) -> Self {
        match x {
            glutin::MouseScrollDelta::LineDelta(a, b) => MouseScrollDelta::LineDelta(a.into(), b.into()),
            glutin::MouseScrollDelta::PixelDelta(a, b) => MouseScrollDelta::PixelDelta(a.into(), b.into()),
        }
    }
}

impl From<glutin::VirtualKeyCode> for VirtualKeyCode { 
    fn from(x: glutin::VirtualKeyCode) -> Self {
        match x {
            glutin::VirtualKeyCode::Key1 => VirtualKeyCode::Key1,
            glutin::VirtualKeyCode::Key2 => VirtualKeyCode::Key2,
            glutin::VirtualKeyCode::Key3 => VirtualKeyCode::Key3,
            glutin::VirtualKeyCode::Key4 => VirtualKeyCode::Key4,
            glutin::VirtualKeyCode::Key5 => VirtualKeyCode::Key5,
            glutin::VirtualKeyCode::Key6 => VirtualKeyCode::Key6,
            glutin::VirtualKeyCode::Key7 => VirtualKeyCode::Key7,
            glutin::VirtualKeyCode::Key8 => VirtualKeyCode::Key8,
            glutin::VirtualKeyCode::Key9 => VirtualKeyCode::Key9,
            glutin::VirtualKeyCode::Key0 => VirtualKeyCode::Key0,
            glutin::VirtualKeyCode::A => VirtualKeyCode::A,
            glutin::VirtualKeyCode::B => VirtualKeyCode::B,
            glutin::VirtualKeyCode::C => VirtualKeyCode::C,
            glutin::VirtualKeyCode::D => VirtualKeyCode::D,
            glutin::VirtualKeyCode::E => VirtualKeyCode::E,
            glutin::VirtualKeyCode::F => VirtualKeyCode::F,
            glutin::VirtualKeyCode::G => VirtualKeyCode::G,
            glutin::VirtualKeyCode::H => VirtualKeyCode::H,
            glutin::VirtualKeyCode::I => VirtualKeyCode::I,
            glutin::VirtualKeyCode::J => VirtualKeyCode::J,
            glutin::VirtualKeyCode::K => VirtualKeyCode::K,
            glutin::VirtualKeyCode::L => VirtualKeyCode::L,
            glutin::VirtualKeyCode::M => VirtualKeyCode::M,
            glutin::VirtualKeyCode::N => VirtualKeyCode::N,
            glutin::VirtualKeyCode::O => VirtualKeyCode::O,
            glutin::VirtualKeyCode::P => VirtualKeyCode::P,
            glutin::VirtualKeyCode::Q => VirtualKeyCode::Q,
            glutin::VirtualKeyCode::R => VirtualKeyCode::R,
            glutin::VirtualKeyCode::S => VirtualKeyCode::S,
            glutin::VirtualKeyCode::T => VirtualKeyCode::T,
            glutin::VirtualKeyCode::U => VirtualKeyCode::U,
            glutin::VirtualKeyCode::V => VirtualKeyCode::V,
            glutin::VirtualKeyCode::W => VirtualKeyCode::W,
            glutin::VirtualKeyCode::X => VirtualKeyCode::X,
            glutin::VirtualKeyCode::Y => VirtualKeyCode::Y,
            glutin::VirtualKeyCode::Z => VirtualKeyCode::Z,
            glutin::VirtualKeyCode::Escape => VirtualKeyCode::Escape,
            glutin::VirtualKeyCode::F1 => VirtualKeyCode::F1,
            glutin::VirtualKeyCode::F2 => VirtualKeyCode::F2,
            glutin::VirtualKeyCode::F3 => VirtualKeyCode::F3,
            glutin::VirtualKeyCode::F4 => VirtualKeyCode::F4,
            glutin::VirtualKeyCode::F5 => VirtualKeyCode::F5,
            glutin::VirtualKeyCode::F6 => VirtualKeyCode::F6,
            glutin::VirtualKeyCode::F7 => VirtualKeyCode::F7,
            glutin::VirtualKeyCode::F8 => VirtualKeyCode::F8,
            glutin::VirtualKeyCode::F9 => VirtualKeyCode::F9,
            glutin::VirtualKeyCode::F10 => VirtualKeyCode::F10,
            glutin::VirtualKeyCode::F11 => VirtualKeyCode::F11,
            glutin::VirtualKeyCode::F12 => VirtualKeyCode::F12,
            glutin::VirtualKeyCode::F13 => VirtualKeyCode::F13,
            glutin::VirtualKeyCode::F14 => VirtualKeyCode::F14,
            glutin::VirtualKeyCode::F15 => VirtualKeyCode::F15,
            glutin::VirtualKeyCode::Snapshot => VirtualKeyCode::Snapshot,
            glutin::VirtualKeyCode::Scroll => VirtualKeyCode::Scroll,
            glutin::VirtualKeyCode::Pause => VirtualKeyCode::Pause,
            glutin::VirtualKeyCode::Insert => VirtualKeyCode::Insert,
            glutin::VirtualKeyCode::Home => VirtualKeyCode::Home,
            glutin::VirtualKeyCode::Delete => VirtualKeyCode::Delete,
            glutin::VirtualKeyCode::End => VirtualKeyCode::End,
            glutin::VirtualKeyCode::PageDown => VirtualKeyCode::PageDown,
            glutin::VirtualKeyCode::PageUp => VirtualKeyCode::PageUp,
            glutin::VirtualKeyCode::Left => VirtualKeyCode::Left,
            glutin::VirtualKeyCode::Up => VirtualKeyCode::Up,
            glutin::VirtualKeyCode::Right => VirtualKeyCode::Right,
            glutin::VirtualKeyCode::Down => VirtualKeyCode::Down,
            glutin::VirtualKeyCode::Back => VirtualKeyCode::Back,
            glutin::VirtualKeyCode::Return => VirtualKeyCode::Return,
            glutin::VirtualKeyCode::Space => VirtualKeyCode::Space,
            glutin::VirtualKeyCode::Numlock => VirtualKeyCode::Numlock,
            glutin::VirtualKeyCode::Numpad0 => VirtualKeyCode::Numpad0,
            glutin::VirtualKeyCode::Numpad1 => VirtualKeyCode::Numpad1,
            glutin::VirtualKeyCode::Numpad2 => VirtualKeyCode::Numpad2,
            glutin::VirtualKeyCode::Numpad3 => VirtualKeyCode::Numpad3,
            glutin::VirtualKeyCode::Numpad4 => VirtualKeyCode::Numpad4,
            glutin::VirtualKeyCode::Numpad5 => VirtualKeyCode::Numpad5,
            glutin::VirtualKeyCode::Numpad6 => VirtualKeyCode::Numpad6,
            glutin::VirtualKeyCode::Numpad7 => VirtualKeyCode::Numpad7,
            glutin::VirtualKeyCode::Numpad8 => VirtualKeyCode::Numpad8,
            glutin::VirtualKeyCode::Numpad9 => VirtualKeyCode::Numpad9,
            glutin::VirtualKeyCode::AbntC1 => VirtualKeyCode::AbntC1,
            glutin::VirtualKeyCode::AbntC2 => VirtualKeyCode::AbntC2,
            glutin::VirtualKeyCode::Add => VirtualKeyCode::Add,
            glutin::VirtualKeyCode::Apostrophe => VirtualKeyCode::Apostrophe,
            glutin::VirtualKeyCode::Apps => VirtualKeyCode::Apps,
            glutin::VirtualKeyCode::At => VirtualKeyCode::At,
            glutin::VirtualKeyCode::Ax => VirtualKeyCode::Ax,
            glutin::VirtualKeyCode::Backslash => VirtualKeyCode::Backslash,
            glutin::VirtualKeyCode::Calculator => VirtualKeyCode::Calculator,
            glutin::VirtualKeyCode::Capital => VirtualKeyCode::Capital,
            glutin::VirtualKeyCode::Colon => VirtualKeyCode::Colon,
            glutin::VirtualKeyCode::Comma => VirtualKeyCode::Comma,
            glutin::VirtualKeyCode::Convert => VirtualKeyCode::Convert,
            glutin::VirtualKeyCode::Decimal => VirtualKeyCode::Decimal,
            glutin::VirtualKeyCode::Divide => VirtualKeyCode::Divide,
            glutin::VirtualKeyCode::Equals => VirtualKeyCode::Equals,
            glutin::VirtualKeyCode::Grave => VirtualKeyCode::Grave,
            glutin::VirtualKeyCode::Kana => VirtualKeyCode::Kana,
            glutin::VirtualKeyCode::Kanji => VirtualKeyCode::Kanji,
            glutin::VirtualKeyCode::LAlt => VirtualKeyCode::LAlt,
            glutin::VirtualKeyCode::LBracket => VirtualKeyCode::LBracket,
            glutin::VirtualKeyCode::LControl => VirtualKeyCode::LControl,
            glutin::VirtualKeyCode::LMenu => VirtualKeyCode::LMenu,
            glutin::VirtualKeyCode::LShift => VirtualKeyCode::LShift,
            glutin::VirtualKeyCode::LWin => VirtualKeyCode::LWin,
            glutin::VirtualKeyCode::Mail => VirtualKeyCode::Mail,
            glutin::VirtualKeyCode::MediaSelect => VirtualKeyCode::MediaSelect,
            glutin::VirtualKeyCode::MediaStop => VirtualKeyCode::MediaStop,
            glutin::VirtualKeyCode::Minus => VirtualKeyCode::Minus,
            glutin::VirtualKeyCode::Multiply => VirtualKeyCode::Multiply,
            glutin::VirtualKeyCode::Mute => VirtualKeyCode::Mute,
            glutin::VirtualKeyCode::MyComputer => VirtualKeyCode::MyComputer,
            glutin::VirtualKeyCode::NavigateForward => VirtualKeyCode::NavigateForward,
            glutin::VirtualKeyCode::NavigateBackward => VirtualKeyCode::NavigateBackward,
            glutin::VirtualKeyCode::NextTrack => VirtualKeyCode::NextTrack,
            glutin::VirtualKeyCode::NoConvert => VirtualKeyCode::NoConvert,
            glutin::VirtualKeyCode::NumpadComma => VirtualKeyCode::NumpadComma,
            glutin::VirtualKeyCode::NumpadEnter => VirtualKeyCode::NumpadEnter,
            glutin::VirtualKeyCode::NumpadEquals => VirtualKeyCode::NumpadEquals,
            glutin::VirtualKeyCode::OEM102 => VirtualKeyCode::OEM102,
            glutin::VirtualKeyCode::Period => VirtualKeyCode::Period,
            glutin::VirtualKeyCode::PlayPause => VirtualKeyCode::PlayPause,
            glutin::VirtualKeyCode::Power => VirtualKeyCode::Power,
            glutin::VirtualKeyCode::PrevTrack => VirtualKeyCode::PrevTrack,
            glutin::VirtualKeyCode::RAlt => VirtualKeyCode::RAlt,
            glutin::VirtualKeyCode::RBracket => VirtualKeyCode::RBracket,
            glutin::VirtualKeyCode::RControl => VirtualKeyCode::RControl,
            glutin::VirtualKeyCode::RMenu => VirtualKeyCode::RMenu,
            glutin::VirtualKeyCode::RShift => VirtualKeyCode::RShift,
            glutin::VirtualKeyCode::RWin => VirtualKeyCode::RWin,
            glutin::VirtualKeyCode::Semicolon => VirtualKeyCode::Semicolon,
            glutin::VirtualKeyCode::Slash => VirtualKeyCode::Slash,
            glutin::VirtualKeyCode::Sleep => VirtualKeyCode::Sleep,
            glutin::VirtualKeyCode::Stop => VirtualKeyCode::Stop,
            glutin::VirtualKeyCode::Subtract => VirtualKeyCode::Subtract,
            glutin::VirtualKeyCode::Sysrq => VirtualKeyCode::Sysrq,
            glutin::VirtualKeyCode::Tab => VirtualKeyCode::Tab,
            glutin::VirtualKeyCode::Underline => VirtualKeyCode::Underline,
            glutin::VirtualKeyCode::Unlabeled => VirtualKeyCode::Unlabeled,
            glutin::VirtualKeyCode::VolumeDown => VirtualKeyCode::VolumeDown,
            glutin::VirtualKeyCode::VolumeUp => VirtualKeyCode::VolumeUp,
            glutin::VirtualKeyCode::Wake => VirtualKeyCode::Wake,
            glutin::VirtualKeyCode::WebBack => VirtualKeyCode::WebBack,
            glutin::VirtualKeyCode::WebFavorites => VirtualKeyCode::WebFavorites,
            glutin::VirtualKeyCode::WebForward => VirtualKeyCode::WebForward,
            glutin::VirtualKeyCode::WebHome => VirtualKeyCode::WebHome,
            glutin::VirtualKeyCode::WebRefresh => VirtualKeyCode::WebRefresh,
            glutin::VirtualKeyCode::WebSearch => VirtualKeyCode::WebSearch,
            glutin::VirtualKeyCode::WebStop => VirtualKeyCode::WebStop,
            glutin::VirtualKeyCode::Yen => VirtualKeyCode::Yen,
        }
    }
}
